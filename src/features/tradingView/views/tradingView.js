import React, { useEffect, useState } from "react";
import styles from "./style.module.css";
import IMG_SEMI_CIRCLE from "../../../assets/images/semiCircle.svg";
const TradingViewPage = () => {
  const dataSource = [22, 5, 14, 78, 59, 99, 23, 25];
  const [process, setProcess] = useState(-90);
  const [index, setIndex] = useState(0);
  const [count, setCount] = useState(0);
  const handleTimer = () => {
    let nextCount = count < 2 ? count + 1 : 0;
    let nextIndex = count === 0 ? index + 1 : index;
    setCount(nextCount);
    setIndex(nextIndex);
  };
  const renderProcessLayout = (process) => {
    console.log("process", process);
    return (
      <>
        <span
          className={styles.arrow}
          style={{ transform: `rotate(${process}deg)` }}
        >
          <span className={styles.arrow_main}></span>
        </span>
        <div className={styles.dots}></div>
      </>
    );
  };
  //fetch data movies
  useEffect(() => {
    const timer = setInterval(() => {
      handleTimer();
    }, 1000);
    if (count === 0) {
      setProcess(index >= dataSource.length ? -90 : 100 - dataSource[index]);
    }
    return () => clearInterval(timer);
  });
  //   detectProcess();
  return (
    <div className={styles.wrapper}>
      <div
        className={styles.circle}
        style={{ background: `url(${IMG_SEMI_CIRCLE}) top no-repeat` }}
      >
        <div className={styles.semi_circle_bg}></div>
        {process ? renderProcessLayout(process) : ""}
      </div>
      <div className={styles.wrapper_content}>
        <div className={styles.wrapper_content_signal_strong}>
          <span className={styles.textContent}>Strong Sell</span>
          <span className={styles.textContent}>Strong Buy</span>
        </div>
        <div className={styles.wrapper_content_signal_normal}>
          <span className={styles.textContent}>Sell</span>
          <span className={styles.textContent}>Buy</span>
        </div>
        <div className={styles.wrapper_content_signal_neutral}>
          <span className={styles.textContent}>Neutral</span>
        </div>
      </div>
    </div>
  );
};

export default TradingViewPage;
