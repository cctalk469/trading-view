import React from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";

import TradingViewPage from "./views/tradingView";

const TradingViewComponents = () => {
  const { url } = useRouteMatch();
  return (
    <Switch>
      <Route path={`${url}`} component={TradingViewPage} />
    </Switch>
  );
};

export default TradingViewComponents;
