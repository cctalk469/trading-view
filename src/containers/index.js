import React from "react";
import { Switch, Route } from "react-router-dom";

import { ROUTES } from "../constants/routes";

import tradingViewComponents from "../features/tradingView";

import { HeaderComponent, FooterComponent } from "../components";
// import { GlobalWrapper } from "../../assets/styles/common";
// import { Reponsiveglobal } from "../../assets/styles/reponsiveGlobal";
const RouterComponents = () => {
  return (
    <>
      <HeaderComponent />
      {/* <GlobalWrapper>
        <Reponsiveglobal /> */}
      <Switch>
        <Route
          exact
          path={`${ROUTES.TRADING_VIEW}`}
          component={tradingViewComponents}
        />
      </Switch>
      {/* </GlobalWrapper> */}
      <FooterComponent />
    </>
  );
};

export default RouterComponents;
