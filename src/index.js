import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { ConnectedRouter } from "connected-react-router";
import configureStore from "./store";
import history from "./utils/history";
import App from "./app/views/App";
import reportWebVitals from "./reportWebVitals";

import { httpService } from "./api/axios";

// Create redux store with history
const initialState = {};
const { store, persistor } = configureStore(initialState, history);

httpService.setupInterceptors(store);
// store.dispatch(handleCheckAuth());

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
