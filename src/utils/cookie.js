//set cookie
function setCookieUser(name, Prop, days) {
  console.log({ name, Prop, days });
  let expires = "";
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + days * 8640000);
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie =
    "__" + name + "=" + (JSON.stringify(Prop) || "") + expires + "; path=/";
}
function getCookie(name) {
  const nameEQ = name + "=";
  const ca = document.cookie.split(";");
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == " ") c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}
export { setCookieUser, getCookie };
