/* eslint-disable no-template-curly-in-string */
import React from "react";
import { ThemeProvider } from "styled-components";
import themes from "../../assets/themes";
import "antd/dist/antd.css";
import Routes from "../../routes";
import "bootstrap/dist/css/bootstrap.css";
import GlobalStyles from "../../assets/styles/globalStyle";
import { LoadingComponents } from "../../components/index";
function App() {
  return (
    <ThemeProvider theme={{ ...themes }}>
      <GlobalStyles />
      <LoadingComponents />
      <Routes />
    </ThemeProvider>
  );
}

export default App;
