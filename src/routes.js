import React, { Suspense, lazy } from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";

import { ErrorBoundary, SuspenseLoading } from "./components";

const RouterComponents = lazy(() => import("./containers"));

export default function Routes() {
  return (
    <ErrorBoundary>
      <Suspense fallback={<SuspenseLoading />}>
        <Router>
          <Switch>
            <RouterComponents />
          </Switch>
        </Router>
      </Suspense>
    </ErrorBoundary>
  );
}
