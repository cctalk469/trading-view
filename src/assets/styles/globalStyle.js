import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
  *{
    box-sizing: inherit;
  }
  html{
    font-size:62.5%;
    box-sizing:border-box;
  }
  h1,
  h2, 
  h3,
  h4,
  h5,
  h6{
    font-weight: 300;
  }
  body{
    font-family:'roboto';
    overflow-x: hidden;
  }
  * a{
  cursor: pointer; 
  }
  // span,a{
  //     display: block;
  // }
  ul{
    padding: 0;
    margin-top: 0;
    margin-bottom: 0;
    li{
        list-style:none;
        a{
            text-decoration:none;
        }
    }
  }
`;

export default GlobalStyles;
