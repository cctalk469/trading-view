const theme = {};

theme.palette = {
  navbar_color: "#000",
  white_color: "#ffff",
  textSpan_color: "#787878",
  orange_color: "#f8971d",

  primary: "#384fa4",
  secondary: "#ff0066",
  background: "#f1f4f8",
  white: "#fff",
  black: "#070708",
  coral500: "#ff8387",
  jonquil: "#ffe530",
  turquoise200: "#d6fffa",
  turquoise500: "#4dccbd",
  robinsEggBlue: "#b2def1",
  violet: "#311495",
  royalBlue100: "#f5f7fa",
  royalBlue150: "#f1f4f8",
  royalBlue400: "#bbc8e1",
  royalBlue700: "#253c78",
  error: "#ff4d4f",
  dark600: "#636369",
  gold: "#fdca40",
  text: {
    default: "#070708",
    title: "#253c78",
  },
  bg: "#f7f9fc",
  border: "#e9e9e9",

  font_weight_body: "600",
  font_weight_bold: "400",
  font_weight_semibold: "300",
  font_weight_light: "200",

  desktop: "1200px",
  tablet_ht: "992px", // Tablet Horizontal
  tablet_vt: "768px", // Tablet Vertical
  mobile: "640px",
  mobile_md: " 480px",
  mobile_sm: "360px",
  mobile_xs: "320px",
};

export default theme;
